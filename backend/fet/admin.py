from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .models import Event, Fet, Order, Provider, Round

admin.site.login = login_required(admin.site.login)


class RoundInline(admin.TabularInline):
    model = Round


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = (RoundInline,)


class FetInline(admin.TabularInline):
    model = Fet


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    inlines = (FetInline,)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_filter = (
        "round",
        "user",
        "user__is_staff",
        "round__event",
        "round__providers",
    )
    list_display = ("round", "user", "fet")
    search_fields = ("user__first_name", "user__last_name", "user__username")
