from django import forms

from .models import Order


class FetSelectionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["user"].required = False
        self.fields["round"].required = False

    class Meta:
        fields = ("fet", "user", "notes", "round")
        model = Order
