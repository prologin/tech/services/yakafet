from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models

User = get_user_model()


class Event(models.Model):
    name = models.CharField(max_length=200)

    groups = models.ManyToManyField(Group)

    def __str__(self):
        return self.name


class Provider(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Round(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    date = models.DateField()

    accept_orders = models.BooleanField(default=True)

    providers = models.ManyToManyField(Provider)

    class Meta:
        ordering = ("-date",)

    def __str__(self):
        return f"{self.event} - {self.date}"


class Fet(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)

    name = models.CharField(max_length=200)
    enabled = models.BooleanField(default=True)
    description = models.TextField(blank=True, default="")

    image = models.FileField(
        upload_to="fet/fets/",
        blank=True,
        null=True,
    )
    image_url = models.URLField(blank=True, default="")

    class Meta:
        ordering = ("provider",)

    def __str__(self):
        return f"{self.provider} - {self.name}"


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    round = models.ForeignKey(
        to="fet.Round", related_name="orders", on_delete=models.CASCADE
    )

    fet = models.ForeignKey(Fet, on_delete=models.CASCADE)

    notes = models.TextField(blank=True, default="")

    class Meta:
        unique_together = ("user", "round")

    def __str__(self):
        return f"{self.round} - {self.user}"
