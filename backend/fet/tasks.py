import requests
from celery import shared_task
from django.conf import settings
from django.contrib.auth.models import Group


@shared_task
def sync_groups():
    response = requests.get(
        f"{settings.AUTHENTIK_ENDPOINT}/core/groups/?page_size=100000",
        headers={"Authorization": f"Bearer {settings.AUTHENTIK_TOKEN}"},
    )

    response.raise_for_status()

    groups = response.json()["results"]

    for group in groups:
        Group.objects.get_or_create(name=group["name"])
