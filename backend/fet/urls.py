from django.urls import path

from .views import (
    RoundOrderSuccessView,
    RoundOrderView,
    RoundProviderRecapView,
    RoundSelectionView,
    RoundUserRecapView,
)

urlpatterns = [
    path("", RoundSelectionView.as_view(), name="index"),
    path(
        "round/<int:pk>/order/", RoundOrderView.as_view(), name="round-order"
    ),
    path(
        "round/<int:pk>/success/",
        RoundOrderSuccessView.as_view(),
        name="round-order-success",
    ),
    path(
        "round/<int:pk>/recap-provider/",
        RoundProviderRecapView.as_view(),
        name="round-recap-provider",
    ),
    path(
        "round/<int:pk>/recap-user/",
        RoundUserRecapView.as_view(),
        name="round-recap-user",
    ),
]
