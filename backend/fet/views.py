from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, FormView, ListView
from django.views.generic.detail import SingleObjectMixin

from .forms import FetSelectionForm
from .models import Fet, Order, Round


class RoundMixin:
    def get_queryset(self):
        qs = Round.objects.filter(date=timezone.now().date())
        if not self.request.user.is_staff:
            qs = qs.filter(
                event__groups__in=self.request.user.groups.all(),
                accept_orders=True,
            )
        return qs


class RoundSelectionView(RoundMixin, LoginRequiredMixin, ListView):
    template_name = "fet/index.html"

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        if self.object_list.count() == 1 and not self.request.user.is_staff:
            return HttpResponseRedirect(
                reverse(
                    "round-order", kwargs={"pk": self.object_list.first().pk}
                )
            )
        return super().get(request, *args, **kwargs)


class RoundOrderView(
    RoundMixin, LoginRequiredMixin, FormView, SingleObjectMixin
):
    template_name = "fet/select.html"
    form_class = FetSelectionForm

    def get_success_url(self):
        return reverse("round-order-success", kwargs={"pk": self.object.pk})

    def order_already_exists(self):
        return Order.objects.filter(
            round=self.object, user=self.request.user
        ).exists()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.order_already_exists():
            return HttpResponseRedirect(self.get_success_url())
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.order_already_exists():
            return HttpResponseRedirect(self.get_success_url())
        return super().post(request, *args, **kwargs)

    def get_fets(self):
        return Fet.objects.filter(
            provider__round=self.object,
            enabled=True,
        ).order_by("provider")

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.fields["fet"].queryset = self.get_fets()
        return form

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.round = self.object
        form.save()
        return super().form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["fets"] = self.get_fets()
        return context


class RoundOrderSuccessView(LoginRequiredMixin, DetailView):
    template_name = "fet/success.html"

    def get_object(self, queryset=None):
        round = super().get_object(queryset=Round.objects.all())
        return get_object_or_404(Order, round=round, user=self.request.user)


class RoundUserRecapView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    template_name = "fet/recap-user.html"

    queryset = Round.objects.all()

    def test_func(self):
        return self.request.user.is_staff

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        orders = (
            Order.objects.filter(
                round=self.object,
            )
            .annotate(order__count=Count("fet"))
            .order_by("user__last_name", "user__first_name")
            .values(
                "user__username",
                "user__first_name",
                "user__last_name",
                "fet__provider__name",
                "fet__name",
            )
        )
        context["orders"] = orders

        return context


class RoundProviderRecapView(
    LoginRequiredMixin, UserPassesTestMixin, DetailView
):
    template_name = "fet/recap-provider.html"

    queryset = Round.objects.all()

    def test_func(self):
        return self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        orders = (
            Order.objects.filter(
                round=self.object,
            )
            .order_by("fet__provider__name")
            .values(
                "fet__provider__name",
                "fet__name",
                "fet__description",
            )
            .annotate(order_count=Count("fet__name"))
        )

        context["orders"] = orders

        return context
