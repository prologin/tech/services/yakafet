# noqa
# pylint: skip-file
from django_utils.celery.app import *

app.conf.beat_schedule["sync-groups"] = {
    "task": "fet.tasks.sync_groups",
    "schedule": 60.0 * 5,  # 5 minutes
}
