from django.urls import include, path
from django_utils.urls import default, drf

urlpatterns = (
    default.urlpatterns()
    + drf.urlpatterns(with_auth=True)
    + [
        path("", include("fet.urls")),
    ]
)
